﻿using Microsoft.EntityFrameworkCore;
using stream.api;

namespace ProjetFinalWeb2.Models
{
	public class DefaultDbContext : DbContext
	{
		public DefaultDbContext(DbContextOptions options) : base(options)
		{

		}

		protected DefaultDbContext()
		{

		}

		/// <summary>
		/// C'est une LISTE ! et elle sera connectée à une table en bdd
		/// </summary>
		public DbSet<stream.api.Stream> Streams { get; set; }
	}
}
