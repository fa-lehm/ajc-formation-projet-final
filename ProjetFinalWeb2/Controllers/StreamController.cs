﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjetFinalWeb2.Models;

namespace ProjetFinalWeb2.Controllers
{
	public class StreamController : Controller
	{
		private readonly DefaultDbContext context;

		public StreamController(DefaultDbContext context)
		{
			this.context = context;
		}

		// GET: StreamController
		public IActionResult List()
		{
			// Prepa requete linq => ça ne fait rien
			var query = from item in this.context.Streams
						orderby item.statut
						select item;

			var modelAEnvoyerALaView = query.ToList(); // Traduction, requete linq en requete sql
			var vueAAppeler = "List";
			return View(vueAAppeler, modelAEnvoyerALaView);
		}

		// GET: StreamController/Details/5
		public ActionResult Details(int id)
		{
			return View();
		}

		// GET: StreamController/Create
		public ActionResult Create()
		{
			return View();
		}

		[HttpGet]
		public IActionResult Add()
		{
			return View();
		}

		// POST: StreamController/Add
		[HttpPost]
		public IActionResult Add(StreamAddViewModel item)
		{
			// date = this.Request.Form["date"];
			//string titre = this.Request.Form["titre"];
			//string statut = this.Request.Form["statut"];

			this.context.Streams.Add(item.UnStream);
			//this.context.Personnes.Add(new Personne());

			this.context.SaveChanges();

			return RedirectToAction("List");
		}

		// GET: StreamController/Edit/5
		public ActionResult Edit(int id)
		{
			return View();
		}

		// POST: StreamController/Edit/5
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit(int id, IFormCollection collection)
		{
			try
			{
				return RedirectToAction(nameof(Index));
			}
			catch
			{
				return View();
			}
		}

		// GET: StreamController/Delete/5
		public ActionResult Delete(int id)
		{
			return View();
		}

		// POST: StreamController/Delete/5
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Delete(int id, IFormCollection collection)
		{
			try
			{
				return RedirectToAction(nameof(Index));
			}
			catch
			{
				return View();
			}
		}
	}
}
