﻿using Microsoft.EntityFrameworkCore;
using stream.api;
using System.Collections.Generic;

namespace ProjetFinalWeb.Models
{
	public class DefaultDbContext : DbContext
	{
		public DefaultDbContext(DbContextOptions options) : base(options)
		{

		}

		protected DefaultDbContext()
		{

		}

		/// <summary>
		/// C'est une LISTE ! et elle sera connectée à une table en bdd
		/// </summary>
		public DbSet<Viewer> Viewers { get; set; }
	}
}
