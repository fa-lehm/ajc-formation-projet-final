﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjetFinalWeb.Models;
using stream.api;
using System.Collections.Generic;

namespace ProjetFinalWeb.Controllers
{
	public class ViewerController : Controller
	{
		private readonly DefaultDbContext context;

		public ViewerController(DefaultDbContext context)
		{
			this.context = context;
		}

		// GET: ViewerController
		public IActionResult List()
		{
			// Prepa requete linq => ça ne fait rien
			var query = from item in this.context.Viewers
						where item.actif.Equals(true)
						select item;

			var modelAEnvoyerALaView = query.ToList(); // Traduction, requete linq en requete sql
			var vueAAppeler = "List";
			return View(vueAAppeler, modelAEnvoyerALaView);
		}

		[HttpGet]
		public IActionResult Add()
		{
			return View();
		}

		// Post: ViewerController/Listfilter
		[HttpPost]
		public ActionResult Listfilter()
		{
			int nbStreamStreamer = int.Parse(this.Request.Form["choix"]);
			var query = from item in this.context.Viewers
						where item.actif.Equals(true) && (item.nbStreamRegardes * 100 / nbStreamStreamer) >= 70
						select item;

			if (nbStreamStreamer == 0)
			{
				// Prepa requete linq => ça ne fait rien
				query = from item in this.context.Viewers
							where item.actif.Equals(true)
							select item;
			} else
			{
			}
			// Prepa requete linq => ça ne fait rien
			var modelAEnvoyerALaView = query.ToList(); // Traduction, requete linq en requete sql
			var vueAAppeler = "List";
			return View(vueAAppeler, modelAEnvoyerALaView);
		}

		// GET: ViewerController/Details/5
		public ActionResult Details(int id)
		{
			return View();
		}

		// GET: ViewerController/Create
		public ActionResult Create()
		{
			return View();
		}

		// POST: ViewerController/Create
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create(IFormCollection collection)
		{
			try
			{
				return RedirectToAction(nameof(Index));
			}
			catch
			{
				return View();
			}
		}

		// GET: ViewerController/Edit/5
		public ActionResult Edit(int id)
		{
			return View();
		}

		// POST: ViewerController/Edit/5
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit(int id, IFormCollection collection)
		{
			try
			{
				return RedirectToAction(nameof(Index));
			}
			catch
			{
				return View();
			}
		}

		// GET: ViewerController/Delete/5
		public ActionResult Delete(int id)
		{
			return View();
		}

		// POST: ViewerController/Delete/5
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Delete(int id, IFormCollection collection)
		{
			try
			{
				return RedirectToAction(nameof(Index));
			}
			catch
			{
				return View();
			}
		}
	}
}
