﻿using stream.api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace stream.api
{
	/// <summary>
	/// Classe Stream représentant un diffusion en direct du streamer
	/// </summary>
	public class Stream
	{
		public int id {  get; set; }
		public string titre { get; set; }
		public DateTime Date { get; set; }
		public List<Viewer> viewers { get; set; }
		public string statut { get; set; }

        public Stream(int id, string titre, string statut)
        {
			this.id = id;
            this.titre = titre;
			this.Date = DateTime.Now;
			this.viewers = new List<Viewer>();
			this.statut = statut;
        }

		public Stream() { }
    }
}
