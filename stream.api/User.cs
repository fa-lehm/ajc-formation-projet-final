﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace stream.api
{
	/// <summary>
	/// La classe abstract User, classe mere de Streamer et de Viewer
	/// </summary>
	public abstract class User
	{
		public int Id { get; init ; }
		public string Pseudo { get; set; }
		public string Email { get; set; }
		public string PhotoUrl { get; set; }

        public User(int id, string pseudo, string email, string photo)
        {
            this.Id = id;
			this.Pseudo = pseudo;
			this.Email = email;
			this.PhotoUrl = photo;
        }
    }
}
