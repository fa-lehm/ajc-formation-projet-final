﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Giveaway.api
{
	/// <summary>
	/// Classe CadeauTempsStreamer heritée de la classe BaseCadeau
	/// </summary>
	public class CadeauTempsStreamer : BaseCadeau
	{
        public CadeauTempsStreamer()
        {
			this.id = 3;
			this.libelle = "Cadeau pour passer du temps avec le Streamer";
		}
	}
}
