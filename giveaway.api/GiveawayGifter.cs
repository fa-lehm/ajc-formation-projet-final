﻿using stream.api;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Giveaway.api
{
	/// <summary>
	/// Classe giveaway qui permet de lancer un tirage au sort aléatoire
	/// pour attribuer ou non un cadeau à des participants.
	/// </summary>
	public class GiveawayGifter
	{
		public List<Viewer> listeViewers;
		public List<BaseCadeau> listeCadeaux;
		private List<int>cadeauxIds = new();
		private int rollResult;

		public GiveawayGifter(List<Viewer> listeViewers, List<BaseCadeau> listeCadeaux) 
		{
			this.listeViewers = listeViewers;
			this.listeCadeaux = listeCadeaux;
			listeCadeaux.ForEach(cadeau =>
			{
				this.cadeauxIds.Add(cadeau.id);
			});
		}
		
		/// <summary>
		/// Fonction permetant de faire un tirage au sort parmi plusieurs nombres aléatoires
		/// représentant les types de cadeaux à gagner
		/// Le 5 correspond à 5 valeurs pour lesquelles il n'y a pas de cadeau à gagner
		/// </summary>
		/// <returns></returns>
		public int Roll()
		{
			Random random = new();
			rollResult = random.Next(1, (listeCadeaux.Count) + 5);
			return rollResult;
		}

		/// <summary>
		/// Fonction permettant de vérifier si le nombre tiré au sort correspond à un cadeau
		/// Si c'est le cas le prenom et email du gagnat est affiché avec le prix remporté
		/// </summary>
		public void DisplayResult()
		{
			listeViewers.ForEach(viewer =>
			{
				Roll();
				if (cadeauxIds.Contains(rollResult))
				{
					var query = from cadeau in listeCadeaux
											   where cadeau.id == rollResult
											   select cadeau;

					foreach (var cadeau in query)
					{
						Console.WriteLine($"{viewer.prenom}, {viewer.email} a gagné 1 X {cadeau.libelle}");
					}
				}
				else
				{
					Console.WriteLine($"{viewer.prenom}, {viewer.email} a perdu");
				}
			});
		}
	}
}
