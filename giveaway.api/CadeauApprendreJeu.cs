﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Giveaway.api
{
	/// <summary>
	/// Classe CadeauApprendreJeu heritée de la classe BaseCadeau
	/// </summary>
	public class CadeauApprendreJeu : BaseCadeau
	{
        public CadeauApprendreJeu()
        {
			this.id = 4;
			this.libelle = "Cadeau pour apprendre à bien jouer à un jeu";
		}
	}
}
