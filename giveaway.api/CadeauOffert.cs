﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Giveaway.api
{
	/// <summary>
	/// Classe CadeauOffert heritée de la classe BaseCadeau
	/// </summary>
	public class CadeauOffert : BaseCadeau
	{
        public CadeauOffert()
        {
            this.id = 2;
            this.libelle = "Cadeau offert (réduc de 100% sur un jeu vidéo)";
		}
	}
}
