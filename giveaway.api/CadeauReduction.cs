﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Giveaway.api
{
	/// <summary>
	/// Classe CadeauReduction heritée de la classe BaseCadeau
	/// </summary>
	public class CadeauReduction : BaseCadeau
	{
		public CadeauReduction() 
		{
			this.id = 1;
			this.libelle = "Cadeau de réduction (promo sur un site de gaming)";
		}
	}
}
