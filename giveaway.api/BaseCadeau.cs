﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Giveaway.api
{
    /// <summary>
    /// Classe Abstract qui donne ses propriétés aux 4 classes filles cadeauX
    /// représentant chacunes un type de cadeau différent
    /// </summary>
	public abstract class BaseCadeau
	{
        public int id;
		public string libelle = "";

        public BaseCadeau()
        {
            this.libelle = "";
        }
    }
}
