﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace menu.api
{
	/// <summary>
	/// Une option du menu
	/// </summary>
	public class MenuItem
	{
		/// <summary>
		/// methode représente la méthode à effectuer lors de la selection dans le menu
		/// </summary>
		private Action methode;
		public int Id { get; set; }

		public string Libelle { get; set; } = "";

		public int OrdreAffichage { get; set; }

		public MenuItem(Action methode)
		{
			this.methode = methode;
		}

		/// <summary>
		/// Executer la methode passée au mMenuItem
		/// </summary>
		public void UtiliserMethode()
		{
			this.methode();
		}

		public override string ToString()
		{
			return $"{this.Id} : {this.Libelle}";
		}
	}
}
