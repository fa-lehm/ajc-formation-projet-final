﻿using stream.api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace stream.api
{
	/// <summary>
	/// Classe Streamer héritée de User
	/// Le streamer est la personne qui diffuse en direct des "stream"
	/// </summary>
	public class Streamer : User
	{
		public List<Stream> streams { get; set; }
		public Streamer(int id, string pseudo, string email, string photo) : base(id, pseudo, email, photo)
		{
		}

		/// <summary>
		/// METHODE A REVOIR
		/// Cette methode calcule le taux de présence moyen des utilisateurs pour la totalité de ses streams
		/// </summary>
		/// <returns></returns>
		public float calculerPoucentage()
		{
			float pourcentage = 0;
			foreach (Stream stream in streams)
			{
				//var nombre = 0;
				stream.viewers.ForEach(viewer =>
				{
					pourcentage = viewer.nbStreamRegardes * 100 / streams.Count;
					Console.WriteLine($" {viewer.prenom} {pourcentage}");
					//return pourcentage;
					//Console.WriteLine(viewer.nbStreamRegardes/streams.Count * 100);
					//nombre++;
				});
				//Console.WriteLine(nombre);
				//return pourcentage;
			}
			return pourcentage;
		}
	}
}
