﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace stream.api
{
    /// <summary>
    /// Classe heritée de User
    /// Le Viewer est la personne qui regarde la diffusion "stream" de streamer
    /// </summary>
    public class Viewer
    {
        public int id { get; private set; }
        public string prenom { get; set; }
        public string email { get; set; }
        /// <summary>
        /// Définit si la personne est connectée actuellement
        /// </summary>
        public bool actif { get ; set; }
        public int nbStreamRegardes { get; set; }

		public Viewer(string prenom, string email)
		{
			this.prenom = prenom;
			this.email = email;
		}
		public Viewer(string prenom, string email, int nbStream)
		{
			this.prenom = prenom;
			this.email = email;
			this.nbStreamRegardes = nbStream;
		}

		public Viewer(int id, string prenom, string email, bool actif, int nbStream)
        {
            this.id = id;
            this.prenom = prenom;
            this.email = email;
            this.actif = actif;
            this.nbStreamRegardes = nbStream;
        }

        public Viewer Ajouter(string prenom, string email)
        {
            Viewer newViewer = new(prenom, email);
            return newViewer;
        }
    }
}
