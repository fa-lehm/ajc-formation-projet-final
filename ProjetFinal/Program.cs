﻿using Giveaway.api;
using menu.api;
using Newtonsoft.Json;
using ProjetFinal.Interfaces;
using stream.api;

Console.WriteLine("Le giveaway va commencer");

List<BaseCadeau> cadeaux = new() {
new CadeauApprendreJeu(),
new CadeauOffert(),
new CadeauReduction(),
new CadeauTempsStreamer()
};

List<Viewer> viewers = new() {
new Viewer("francois", "francois@email", 2),
new Viewer("ernesto", "ernesto@email", 2),
new Viewer("pepito", "pepito@email", 1),
new Viewer("arouf", "arouf@email", 3),
new Viewer("marco", "marco@email", 1),
};

stream.api.Stream stream1 = new(1, "Stream du 11/12", "oui");
stream.api.Stream stream2 = new(2, "Stream du 13/12", "");
stream.api.Stream stream3 = new(3, "Stream du 14/12", "");


List<stream.api.Stream> listeDeStream = new();
listeDeStream.Add(stream1);
listeDeStream.Add(stream2);
listeDeStream.Add(stream3);

Streamer streamer = new(1, "pseudo", "email", "photo");
streamer.streams = listeDeStream;

streamer.calculerPoucentage();

GiveawayGifter giveaway = new(viewers, cadeaux);
void LancerLeGiveaway()
{
	giveaway.DisplayResult();
}

Menu menu = new(mess =>
{
	Console.ForegroundColor = ConsoleColor.DarkGreen;
	Console.WriteLine(mess);
	Console.ForegroundColor = ConsoleColor.White;
},
Console.ReadLine);

menu.Ajouter(new MenuItem(AjouterFollower) { Id = 1, Libelle = "Ajouter un follower", OrdreAffichage = 1 });
menu.Ajouter(new MenuItem(ListerCadeaux) { Id = 2, Libelle = "Lister les cadeaux", OrdreAffichage = 2 });
menu.Ajouter(new MenuItem(AjouterNouveauCadeau) { Id = 3, Libelle = "Ajouter un nouveau cadeau", OrdreAffichage = 3 });
menu.Ajouter(new MenuItem(SauvegarderEnFichier) { Id = 4, Libelle = "Sauvegarder en fichier", OrdreAffichage = 4 });
menu.Ajouter(new MenuItem(LancerLeGiveaway) { Id = 5, Libelle = "Lancer le giveaway", OrdreAffichage = 5 });
menu.Ajouter(new MenuItem(Quitter) { Id = 0, Libelle = "Quitter", OrdreAffichage = 0 });

void AjouterFollower()
{
	Console.WriteLine("Ajouter un follower");
	Console.WriteLine("prenom : ");
	var prenom = Console.ReadLine();
	Console.WriteLine("email : ");
	var email = Console.ReadLine();
	Viewer newViewer = new(prenom, email);
	viewers.Add(newViewer);
}

void ListerCadeaux()
{
	var i = 1;
	cadeaux.ForEach(cadeau =>
	{
		Console.WriteLine($"{i} : {cadeau.libelle}");
		i++;
	});
}

void AjouterNouveauCadeau()
{
	Console.WriteLine("Ajouter un cadeau");
	Console.WriteLine("Quel type de cadeau ? : 1 / 2 / 3 / 4");
	int choix = int.Parse(Console.ReadLine());

	switch (choix)
	{
		case 1:
			CadeauReduction objet1 = new();
			cadeaux.Add(objet1);
			break;
		case 2:
			CadeauOffert objet2 = new();
			cadeaux.Add(objet2);
			break;
		case 3:
			CadeauTempsStreamer objet3 = new();
			cadeaux.Add(objet3);
			break;
		case 4:
			CadeauApprendreJeu objet4 = new();
			cadeaux.Add(objet4);
			break;
		default:
			Console.WriteLine("Option invalide");
			break;
	}
}

void SauvegarderEnFichier()
{
	var cheminFichier = Path.Combine(Environment.CurrentDirectory, "test.txt");
	string jsonCadeaux = JsonConvert.SerializeObject(cadeaux, Formatting.Indented);

	try
	{
		File.WriteAllText(cheminFichier, jsonCadeaux);
	}
	catch (PathTooLongException ex)
	{
		Console.WriteLine(ex.Message);
	}
	catch (IOException ex)
	{
		Console.WriteLine("erreur 2");
	}
}

void SelectionMenu()
{
	bool showMenu = true;
	int userInput;

	while (showMenu)
	{
		menu.Afficher();
		Console.WriteLine("Ton choix ?");

		if (int.TryParse(Console.ReadLine(), out userInput))
		{
			if (userInput == 0) { showMenu = false; }
			else
			{
				menu.Items.Find(item => item.Id == userInput)?.UtiliserMethode();
			}
		}
		else
		{
			Console.WriteLine("Choix non valide");
		}
	}
}

void Quitter()
{

}

SelectionMenu();